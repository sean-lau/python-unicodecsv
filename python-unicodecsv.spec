%global _empty_manifest_terminate_build 0
Name:		python-unicodecsv
Version:	0.14.1
Release:	1
Summary:	Python2's stdlib csv module is nice, but it doesn't support unicode. This module is a drop-in replacement which *does*.
License:	BSD License
URL:		https://github.com/jdunck/python-unicodecsv
Source0:	https://files.pythonhosted.org/packages/6f/a4/691ab63b17505a26096608cc309960b5a6bdf39e4ba1a793d5f9b1a53270/unicodecsv-0.14.1.tar.gz
BuildArch:	noarch
%description
unicodecsv
==========

The unicodecsv is a drop-in replacement for Python 2.7's csv module which supports unicode strings without a hassle.  Supported versions are python 2.7, 3.3, 3.4, 3.5, and pypy 2.4.0.

More fully
----------

Python 2's csv module doesn't easily deal with unicode strings, leading to the dreaded "'ascii' codec can't encode characters in position ..." exception.

You can work around it by encoding everything just before calling write (or just after read), but why not add support to the serializer?

.. code-block:: pycon

   >>> import unicodecsv as csv
   >>> from io import BytesIO
   >>> f = BytesIO()
   >>> w = csv.writer(f, encoding='utf-8')
   >>> _ = w.writerow((u'é', u'ñ'))
   >>> _ = f.seek(0)
   >>> r = csv.reader(f, encoding='utf-8')
   >>> next(r) == [u'é', u'ñ']
   True

Note that unicodecsv expects a bytestream, not unicode -- so there's no need to use `codecs.open` or similar wrappers.  Plain `open(..., 'rb')` will do.

(Version 0.14.0 dropped support for python 2.6, but 0.14.1 added it back.  See c0b7655248c4249 for the mistaken, breaking change.)

%package -n python2-unicodecsv
Summary:	Python2's stdlib csv module is nice, but it doesn't support unicode. This module is a drop-in replacement which *does*.
Provides:	python-unicodecsv
BuildRequires:	python2-devel
BuildRequires:	python2-setuptools
%description -n python2-unicodecsv
unicodecsv
==========

The unicodecsv is a drop-in replacement for Python 2.7's csv module which supports unicode strings without a hassle.  Supported versions are python 2.7, 3.3, 3.4, 3.5, and pypy 2.4.0.

More fully
----------

Python 2's csv module doesn't easily deal with unicode strings, leading to the dreaded "'ascii' codec can't encode characters in position ..." exception.

You can work around it by encoding everything just before calling write (or just after read), but why not add support to the serializer?

.. code-block:: pycon

   >>> import unicodecsv as csv
   >>> from io import BytesIO
   >>> f = BytesIO()
   >>> w = csv.writer(f, encoding='utf-8')
   >>> _ = w.writerow((u'é', u'ñ'))
   >>> _ = f.seek(0)
   >>> r = csv.reader(f, encoding='utf-8')
   >>> next(r) == [u'é', u'ñ']
   True

Note that unicodecsv expects a bytestream, not unicode -- so there's no need to use `codecs.open` or similar wrappers.  Plain `open(..., 'rb')` will do.

(Version 0.14.0 dropped support for python 2.6, but 0.14.1 added it back.  See c0b7655248c4249 for the mistaken, breaking change.)

%package help
Summary:	Development documents and examples for unicodecsv
Provides:	python2-unicodecsv-doc
%description help
unicodecsv
==========

The unicodecsv is a drop-in replacement for Python 2.7's csv module which supports unicode strings without a hassle.  Supported versions are python 2.7, 3.3, 3.4, 3.5, and pypy 2.4.0.

More fully
----------

Python 2's csv module doesn't easily deal with unicode strings, leading to the dreaded "'ascii' codec can't encode characters in position ..." exception.

You can work around it by encoding everything just before calling write (or just after read), but why not add support to the serializer?

.. code-block:: pycon

   >>> import unicodecsv as csv
   >>> from io import BytesIO
   >>> f = BytesIO()
   >>> w = csv.writer(f, encoding='utf-8')
   >>> _ = w.writerow((u'é', u'ñ'))
   >>> _ = f.seek(0)
   >>> r = csv.reader(f, encoding='utf-8')
   >>> next(r) == [u'é', u'ñ']
   True

Note that unicodecsv expects a bytestream, not unicode -- so there's no need to use `codecs.open` or similar wrappers.  Plain `open(..., 'rb')` will do.

(Version 0.14.0 dropped support for python 2.6, but 0.14.1 added it back.  See c0b7655248c4249 for the mistaken, breaking change.)

%prep
%autosetup -n unicodecsv-0.14.1

%build
%py2_build

%install
%py2_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python2-unicodecsv -f filelist.lst
%dir %{python2_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Apr 30 2021 OpenStack_SIG <openstack@openeuler.org>
- Package Spec generated
